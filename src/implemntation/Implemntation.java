/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package implemntation;
import implemntation.Exception.CartItemNotFoundException;
import implemntation.persistencelayer.FilePersistence;
import implemntation.shoppincart.CartItem;
import implemntation.shoppincart.IShoppingCart ;
import implemntation.shoppincart.ICartItem ;
import implemntation.shoppincart.ShoppingCart;
import java.util.Scanner;
import java.util.Vector;
import implemntation.persistencelayer.IPersistenceMechanism;
/**
 *
 * @author Radwa
 */
public class Implemntation {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws CartItemNotFoundException {
        // TODO code application logic here
        Scanner sc = new Scanner (System.in);
        int  ID = 1;
        IShoppingCart shopping = new ShoppingCart();
        IPersistenceMechanism p = new FilePersistence ();
        while (true){
            System.out.println("1- Add item ?");
            System.out.println("2- Update quantity ?");
            System.out.println("3- Remove Cart item ?");
            System.out.println("4- Retrieve List of Cart items  ?");
            System.out.println("5- Retrieve item ?");
            System.out.println("6- informtion about the cart  ?");
            System.out.println("7- Save ");
            System.out.println("8- Load ");
            int choice = sc.nextInt();
            if (choice == 1 ){
                ICartItem tmp = new CartItem();
                tmp.setId(ID);
                System.out.println("Product ID : ");
                tmp.setProductId(sc.nextInt());
                System.out.println("Quantity : ");
                tmp.setQuantity(sc.nextInt());
                System.out.println("Unit Price : ");
                tmp.setUnitPrice(sc.nextInt());
                shopping.addItem(tmp);
                System.out.println("done");
                ID ++;
            }else if (choice == 2){
                System.out.println("Cart ID ?");
                int id = sc.nextInt();
                System.out.println("Quantity ?");
                shopping.getItem(id).setQuantity(sc.nextInt());
                System.out.println("done");
            }else if (choice == 3){
                System.out.println("Cart ID ?");
                shopping.removeItem(sc.nextInt());
                System.out.println("done");
            }else if (choice == 4){
                Vector <ICartItem> items = new Vector <ICartItem>();
                items = shopping.getItems();
                int j ;
                for (int i=0;i<items.size();i++){
                    j=i+1;
                    System.out.println("Cart " + j + " : ");
                    System.out.println("    Product Id : " + items.get(i).getProductId());
                    System.out.println("    Quantity Id : " + items.get(i).getQuantity());
                    System.out.println("    Unit price : " + items.get(i).getUnitPrice());
                    System.out.println("    Total Cost : " + items.get(i).getTotalCost());
                }
                System.out.println("done");
            }else if (choice == 5){
                ICartItem item ;
                System.out.println("Cart ID ?");
                int id = sc.nextInt();
                item = shopping.getItem(id);
                System.out.println("Cart " + id + " : ");
                System.out.println("    Product Id : " + item.getProductId());
                System.out.println("    Quantity Id : " + item.getQuantity());
                System.out.println("    Unit price : " + item.getUnitPrice());
                System.out.println("    Total Cost : " + item.getTotalCost());
            }else if (choice == 6){
                System.out.println("Total number of items = "+shopping.countItems());
                System.out.println("Total Price of the Cart = " + shopping.Totalprice());
                System.out.println("Last access date = " + shopping.getLastAccessedDate());
                System.out.println("Status = OPen " );
                System.out.println("Shopping Cart ID = " + shopping.getId());
                System.out.println("Customer ID = "+shopping.getCustomerID());
                System.out.println("Session ID = " + shopping.getSessionID());
            }else if (choice == 7 ){
                p.save(shopping);
            }else if (choice == 8 ){
                System.out.println("Shopping Cart ID ?");
                int id = sc.nextInt();
                shopping = p.lodaCard(id);
            }
        }
    }
}
