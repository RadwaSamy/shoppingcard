package implemntation.persistencelayer;

public class PersistenceFactory {

 
    /**
     * Generate an object depend on the type of the parametar (SQL or File )
     * @param type  of string
     * @return IPersistenceMechanism (SQL or File )
     */
  public IPersistenceMechanism loadMechanism(String type) {
  
      if (type.equals("File")){
          return new FilePersistence();
      }
      else if (type.equals("SQL"))
      return new SQLPersistence();
      else 
      return null;    
  }

}