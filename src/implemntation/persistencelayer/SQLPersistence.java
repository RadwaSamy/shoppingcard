
package implemntation.persistencelayer;

import implemntation.shoppincart.IShoppingCart;


public class SQLPersistence implements IPersistenceMechanism{

    /**
     *take  a shopping cart and save it's data on the SQL database 
     * @param cart type IShoppingCart
     */
    
    @Override
    public void save(IShoppingCart cart) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    /**
     *  take card id an load it's data from the SQL database 
     * @param CardID
     * @return IShoppingCart
     */
    
    @Override
    public IShoppingCart lodaCard(int CardID) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    /**
     * create new shopping cart
     * @param sessionID
     * @param customerID
     * @return IShoppingCart
     */

    @Override
    public IShoppingCart createCart(int sessionID, int customerID) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    /**
     * remove shopping cart from the SQL database
     * @param cart type of IShoppingCart
     */

    @Override
    public void removeCart(IShoppingCart cart) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   
    /**
     * return an object of a type SQLPersistence
     * @return SQLPersistence object
     */
    @Override
    public IPersistenceMechanism getInstance() {
        return new SQLPersistence();
    }
    
}
