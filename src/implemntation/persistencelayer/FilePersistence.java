
package implemntation.persistencelayer;

import implemntation.Exception.CartItemNotFoundException;
import implemntation.shoppincart.CartItem;
import implemntation.shoppincart.IShoppingCart;
import implemntation.shoppincart.ShoppingCart;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FilePersistence implements IPersistenceMechanism{

    
/**
 * @param cart type of IShoppingCart
 *  method for save shopping cart it take IShoppingCart and save it on a file 
 * @return void 
 */
    
    @Override
    public void save(IShoppingCart cart) {
    // write shopping card    
   try{
  
       
       
        FileWriter fstream = new FileWriter(Integer.toString(cart.getId())+".txt");
        BufferedWriter out = new BufferedWriter(fstream);
        out.write(cart.getId()+" ");
        out.write(cart.getCustomerID()+" ");
        out.write(cart.getSessionID()+" ");
        out.write(cart.countItems()+" ");
        
        for (int i=0;i<cart.countItems();i++){
         out.write(cart.getItems().elementAt(i).getId()+" ");
         out.write(cart.getItems().elementAt(i).getProductId()+" "); 
         out.write(cart.getItems().elementAt(i).getQuantity()+" "); 
         out.write(cart.getItems().elementAt(i).getUnitPrice()+" ");
          
        }
        
        out.close();
            
  }catch (IOException e){//Catch exception if any
  System.err.println("Error: " + e.getMessage());
  }
  
    }

 /**
 * @param CardID type of integer's 
 * method for load  shopping cart it take card id and load it from a file 
 * @return IShoppingCart 
 */
    
    
    
    
    @Override
    public IShoppingCart lodaCard(int CardID) {
        ShoppingCart mCart = new ShoppingCart();
         File file = new File(Integer.toOctalString(CardID)+".txt");      
        try { 
            Scanner cin = new Scanner(file);
            mCart.id=cin.nextInt();
            mCart.customerId=cin.nextInt();
            mCart.sessionId=cin.nextInt();
            int nOfitems=cin.nextInt();
            for (int i=0;i<nOfitems;i++){
                CartItem m= new  CartItem();
                m.id=cin.nextInt();
                m.productId=cin.nextInt();
                m.quantity=cin.nextInt();
                m.unitprice=cin.nextDouble();
                mCart.addItem(m);
            }
            
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (CartItemNotFoundException ex) {
            Logger.getLogger(FilePersistence.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return mCart;
    }

    
    
 /**
 * @param sessionID type of integer's 
 * @param customerID type of integer's 
 * method for    create new shopping cart
 * @return IShoppingCart 
 */
    
    
     @Override
    public IShoppingCart createCart(int sessionID, int customerID) {
        ShoppingCart mCart = new ShoppingCart();
        mCart.customerId=customerID;
        mCart.sessionId=sessionID;
        return mCart;
    }

    
 /**
 * @param cart type of IShoppingCart 
 * method for remove the file of the given cart 
 */
    
   
    @Override
    public void removeCart(IShoppingCart cart) {
        
    try{
 
    		File file = new File(cart.getId()+".txt");
 
    		if(file.delete()){
    			System.out.println(file.getName() + " is deleted!");
    		}else{
    			System.out.println("Delete operation is failed.");
    		}
 
    	}catch(Exception e){
 
    		e.getMessage();
 
    	}
 
    }
/**
 * method for return an object of type FilePersistence
 * @return FilePersistence
 */    
  
    @Override
    public IPersistenceMechanism getInstance() {
        return new FilePersistence();
    }
    
}
