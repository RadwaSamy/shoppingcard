
package implemntation.persistencelayer;

import implemntation.shoppincart.IShoppingCart;

public interface IPersistenceMechanism {
    
   // interface class of the presistence layer which contain save , loadcard , creatCard and reomove card
            
    public void save(IShoppingCart cart);
    public IShoppingCart lodaCard (int CardID);
    public IShoppingCart createCart( int sessionID ,int customerID  );
    public void removeCart( IShoppingCart cart);
    public IPersistenceMechanism getInstance(); 
}
