package implemntation.Exception;

public class PersistenceException extends java.lang.Exception {

  String message;
   
  /**
   * set message for PersistenceException
   * @param msg 
   */
  
  public void setMessage(String msg) {
      message=msg;
  }
/**
 * return message of PersistenceException
 * @return message of PersistenceException
 */
    @Override
  public String getMessage() {
    
    return message;
  }

}