package implemntation.Exception;
public class CartItemNotFoundException extends java.lang.Exception {
    
    
   /**
    * contractor  for CartItemNotFoundException 
    * @param msg 
    */ 
  public CartItemNotFoundException(String msg){
      super(msg);
  }
  
  
  /**
   * 
   * @return CartItemNotFoundException message 
   */
  public String getMessage() {
    return super.getMessage();
  }
}
