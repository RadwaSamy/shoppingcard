package implemntation.shoppincart;

import java.util.Date;
import java.util.Vector;
public interface IShoppingCart {

  public int getId();

  public int getSessionID();

  public int getCustomerID();
  
  public void setId(int id);

  public void setSessionID(int sid);

  public void setCustomerID(int cid);
  
  public String getStatus ();
  
  public double Totalprice ();
  
  public void addItem(ICartItem item) throws implemntation.Exception.CartItemNotFoundException ;

  public void updateQuantity(int cartItemID, int newQuantity) throws implemntation.Exception.CartItemNotFoundException;

  public void removeItem(int cartItemId)throws implemntation.Exception.CartItemNotFoundException;

  public ICartItem getItem(int productID)throws implemntation.Exception.CartItemNotFoundException;

  public Vector <ICartItem> getItems();

  public int countItems();

  public Date getLastAccessedDate();

  public static class ShoppingCart {

    public ShoppingCart() {
        
    }
  }
}