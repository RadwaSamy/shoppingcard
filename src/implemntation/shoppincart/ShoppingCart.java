package implemntation.shoppincart;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

public class ShoppingCart implements IShoppingCart {

  public int id;

  public int sessionId;

  public int customerId;
  
  public String Status;
  
  public Vector <ICartItem> items;

  public Date lastAccessed;

  public ICartItem myICartItem;
  
  public Calendar calender;
  //public Vector  throwe;
  @Override
  public int getId() {
    UpdateDate();
    return id;
  }
  @Override
  public String getStatus (){
      return Status;
  }
  @Override
  public int getSessionID() {
    UpdateDate();
    return sessionId;
  }
  @Override
  public int getCustomerID() {
    UpdateDate();
    return customerId;
  }
  @Override
  public void setId(int id) {
    UpdateDate();
    this.id = id;
  }
  @Override
  public void setSessionID(int sid) {
    UpdateDate();
    this.sessionId = sid;
  }
  @Override
  public void setCustomerID(int cid) {
    UpdateDate();
    this.customerId = cid;
  }
  /**
   * Adds a new item to the Vector 
   * @param item
   * @throws implemntation.Exception.CartItemNotFoundException when the new Cart has a product id already exists
   */
  @Override
  public void addItem(ICartItem item) throws implemntation.Exception.CartItemNotFoundException{      
      for (int i=0;i<items.size();i++){
          if (item.getProductId() == items.get(i).getProductId()){
              throw  new implemntation.Exception.CartItemNotFoundException("this product already exists ");
          }
      }
      UpdateDate();
      items.add(item);
  }
  /**
   * Updates the Quantity of a Cart item with this id 
   * @param cartItemID
   * @param newQuantity
   * @throws implemntation.Exception.CartItemNotFoundException when the cart item is not exist 
   */
  @Override
  public void updateQuantity(int cartItemID, int newQuantity) throws implemntation.Exception.CartItemNotFoundException
  {
      boolean found=false;
      for (int i=0;i<items.size();i++){
          if (items.get(i).getId() == cartItemID ){
              items.get(i).setQuantity(newQuantity);
              found=true;
          }
      }
      UpdateDate();
      if (!found)
          throw new implemntation.Exception.CartItemNotFoundException("item not found ");
  }
  /**
   * Removes item from cart items vector 
   * @param cartItemId
   * @throws implemntation.Exception.CartItemNotFoundException when the item is not exist
   */
  @Override
  public void removeItem(int cartItemId) throws implemntation.Exception.CartItemNotFoundException{
      UpdateDate();
      for (int i=0;i<items.size();i++){
          if (items.get(i).getId()== cartItemId){
              items.remove(items.get(i));
              return ;
          }
      }
      throw new implemntation.Exception.CartItemNotFoundException("item not found ");
  }
  /**
   * @param productID
   * @return Cart item with this product ID 
   * @throws implemntation.Exception.CartItemNotFoundException when this product id is not in any of those Carts in the vector 
   */
  @Override
  public ICartItem getItem(int productID) throws implemntation.Exception.CartItemNotFoundException{
      for (int i=0;i<items.size();i++){
          if (items.get(i).getProductId()==productID){
              UpdateDate();
              return items.get(i);
          }
      }
      UpdateDate();
      throw new implemntation.Exception.CartItemNotFoundException("item not found ");
  }
  /**
   * Calculates the total price of this Shopping Cart by get the summation of total cost of the Cart items
   * @return double Total price
   */
  @Override
  public double Totalprice (){
      double total = 0;
      for (int i=0;i<items.size();i++){
          total+=items.get(i).getTotalCost();
      }
      return total ;
  }
  @Override
  public Vector <ICartItem> getItems() {
    UpdateDate();
    return items;
  }
  @Override
  public int countItems() {
      UpdateDate();
      return items.size();
  }
  @Override
  public Date getLastAccessedDate() {
    return lastAccessed;
  }
  
  public void UpdateDate (){
      lastAccessed = new Date();
  }
  
  public ShoppingCart(int cartID, int customerID, int sessionID) {
      this.id = cartID;
      this.customerId = customerID;
      this.sessionId = sessionID;
      items = new Vector<ICartItem> ();
      ICartItem tmp = new CartItem();
      items.add(tmp);
      UpdateDate();
  }
  public ShoppingCart() {
      items = new Vector<ICartItem> ();
      Status = "Open" ;
      UpdateDate();
  }
}