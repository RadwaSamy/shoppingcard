package implemntation.shoppincart;
public class CartItem implements ICartItem {

  public int id;
  public int productId;
  public int quantity;
  public double price;
  public double unitprice;
  /**
   * Returns the ID of this Cart item 
   * @return integer ID
   */
  @Override
  public int getId() {
    return id;
  }
  /**
   * Returns Product ID of this Cart item 
   * @return integer product ID 
   */
  @Override
  public int getProductId() {
    return productId;
  }
  /**
   * Returns the Quantity of the product in this cart item 
   * @return integer Quantity
   */
  @Override
  public int getQuantity() {
    return quantity;
  }
  /**
   * Returns unit price of cart item's product
   * @return double unit price 
   */
  @Override
  public double getUnitPrice() {
    return unitprice;
  }
  /**
   * Returns the total cost of the Quantity
   * @return double unit price * quantity
   */
  @Override
  public double getTotalCost() {
    return unitprice * quantity;
  }
  /**
   * Sets the id of the Cart item 
   * @param id 
   */
  @Override
  public void setId(int id) {
      this.id=id;
  }
  /**
   * Sets the Product's ID 
   * @param id 
   */
  @Override
  public void setProductId(int id) {
      productId = id;
  }
  /**
   * Sets the Quantity 
   * @param quantity
   * @throws implemntation.Exception.IlegalQuantityException when the Quantity is less than 1 
   */
  @Override
  public void setQuantity(int quantity) throws implemntation.Exception.IlegalQuantityException {
      if (quantity <= 0 ) throw new implemntation.Exception.IlegalQuantityException("Ilegal Quantity");
      this.quantity=quantity;
  }
  /**
   * Set the unit price 
   * @param price 
   */
  @Override
  public void setUnitPrice(double price) {
      this.unitprice=price;
  }
  /**
   * Constructor Sets the data and calls setQuantity method
   * @param id
   * @param productId
   * @param quantity
   * @param price 
   */
  public void CartItem(int id, int productId, int quantity, double price) {
      this.id = id;
      this.price = price;
      setQuantity(quantity);
      this.productId = productId;
  }
}