package implemntation.shoppincart;
public interface ICartItem {

    /**
   * 
   * @element-type ShoppingCart
   */
  //public Vector <ShoppingCart> myShoppingCart;

  public int getId();

  public int getProductId();

  public int getQuantity();

  public double getUnitPrice();

  public double getTotalCost();

  public void setId(int id);

  public void setProductId(int id);

  public void setQuantity(int quantity) throws implemntation.Exception.IlegalQuantityException;

  public void setUnitPrice(double price);

}