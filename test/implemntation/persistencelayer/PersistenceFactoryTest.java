/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package implemntation.persistencelayer;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author tariqsenosy
 */
public class PersistenceFactoryTest {/**
     * Test of loadMechanism method, of class PersistenceFactory.
     */
    @Test
    public void testLoadMechanism() {
        
        PersistenceFactory instance = new PersistenceFactory();
        IPersistenceMechanism expResult = new FilePersistence();
        IPersistenceMechanism result = instance.loadMechanism("File");
        if(expResult.getClass().equals(result.getClass()))// if the same class 
        assertEquals(1, 1);
    }
    
}
