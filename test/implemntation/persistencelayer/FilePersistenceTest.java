/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package implemntation.persistencelayer;

import implemntation.shoppincart.IShoppingCart;
import implemntation.shoppincart.ShoppingCart;
import junit.framework.TestCase;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author tariqsenosy
 */
public class FilePersistenceTest extends TestCase{
    private Object FilePersistenceObj;
    
    public FilePersistenceTest() {
    
    }
    
    @BeforeClass
    public static void setUpClass() {
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        FilePersistence  FilePersistenceObj;
        FilePersistenceObj = new FilePersistence();
    }
    
    @After
    public void tearDown() {
    }

/*     * Test of createCart method, of class FilePersistence.
     */
    @Test
    public void testCreateCart() {
        
        FilePersistence FilePersistenceObj;
        FilePersistenceObj= new FilePersistence();
        int sessionId=1;
        int userId=2;
        ShoppingCart expectedCard = new ShoppingCart();
        expectedCard.customerId=userId;
        expectedCard.sessionId=sessionId;
        IShoppingCart Res;
        Res = FilePersistenceObj.createCart( sessionId,  userId);
        if((expectedCard.customerId==Res.getCustomerID())&&(expectedCard.sessionId==Res.getSessionID()))
        assertEquals(1,1);
    }

}
