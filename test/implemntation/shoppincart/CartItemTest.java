/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package implemntation.shoppincart;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author tariqsenosy
 */
public class CartItemTest {
    
    public CartItemTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getId method, of class CartItem.
     */
    @Test
    public void testGetId() {
        
        CartItem instance = new CartItem();
        int expResult = 0;
        int result = instance.getId();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getProductId method, of class CartItem.
     */
    @Test
    public void testGetProductId() {
        CartItem instance = new CartItem();
        int expResult = 0;
        int result = instance.getProductId();
        assertEquals(expResult, result);
    }

    /**
     * Test of getQuantity method, of class CartItem.
     */
    @Test
    public void testGetQuantity() {
        CartItem instance = new CartItem();
        int expResult = 0;
        int result = instance.getQuantity();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getUnitPrice method, of class CartItem.
     */
    @Test
    public void testGetUnitPrice() {
        CartItem instance = new CartItem();
        double expResult = 0.0;
        double result = instance.getUnitPrice();
        assertEquals(expResult, result, 0.0);
      }

    /**
     * Test of getTotalCost method, of class CartItem.
     */
    @Test
    public void testGetTotalCost() {
        CartItem instance = new CartItem();
        double expResult = 0.0;
        double result = instance.getTotalCost();
        assertEquals(expResult, result, 0.0);
        
    }

    /**
     * Test of setId method, of class CartItem.
     */
    @Test
    public void testSetId() {
        System.out.println("setId");
        int id = 10;
        CartItem instance = new CartItem();
        instance.setId(id);
     assertEquals(10, instance.getId());
        
    }

    /**
     * Test of setProductId method, of class CartItem.
     */
    @Test
    public void testSetProductId() {
        int id =10;
        CartItem instance = new CartItem();
        instance.setProductId(id);
        assertEquals(id,instance.getProductId());
    }

    /**
     * Test of setQuantity method, of class CartItem.
     */
    @Test
    public void testSetQuantity() {
        System.out.println("setQuantity");
        int quantity =20;
        CartItem instance = new CartItem();
        instance.setQuantity(quantity);
        assertEquals(quantity,instance.getQuantity());
    }

    /**
     * Test of setUnitPrice method, of class CartItem.
     */
    @Test
    public void testSetUnitPrice() {
        double price = 0.0;
        CartItem instance = new CartItem();
        instance.setUnitPrice(price);
       
    }

  
    
}
